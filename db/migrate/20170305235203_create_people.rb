class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :name
      t.date :birthdate
      t.string :street
      t.string :city
      t.string :state
      t.string :zip
      t.text :bio
      t.boolean :newsletter

      t.timestamps
    end
  end
end
